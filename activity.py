# S02 Activity:
# 1. Get a year from the user and determine if it is a leap year or not.
num1 = int(input("Enter a year: "))

if num1 % 4 == 0:
    if num1 % 100 == 0:
        if num1 % 400 == 0:
            print(f"{num1} is a leap year.")
        else:
            print(f"{num1} is not a leap year.")
    else:
        print(f"{num1} is a leap year.")
else:
    print(f"{num1} is not a leap year.")

# Input automatically assigns input as a string. To solve this, we can convert the data type using int()
# num1 = int(input("Enter 1st number:"))
# num2 = int(input("Enter 2nd number:"))
# print(f"The sum of num1 and num2 is {num1 + num2}")
# 2.  Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.
rows = int(input("Enter rows: "))
columns = int(input("Enter columns: "))

for x in range(rows):
    for y in range(columns):
        print("*", end="")
    print()